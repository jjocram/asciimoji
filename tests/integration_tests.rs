use asciimoji;
use std::env::var;

#[test]
fn print_shrug() {
    let config_home = var("XDG_CONFIG_HOME").or_else(|_| var("HOME").map(|home|format!("{}/.asciimojis", home)));
    let config_home = config_home.unwrap_or(String::from("asciimojis.yml"));


    match asciimoji::downloader::download_yaml_file_from_repository(config_home.as_str()) {
        Ok(_) => {},
        Err(e) => println!("Errore: {}", e)
    }

    let mut asciimojis: Vec<asciimoji::Asciimoji> = asciimoji::read_asciimojis_from_file(config_home).unwrap();
    asciimojis = asciimojis
        .into_iter()
        .map(|asciimoji| asciimoji.names_to_lowercase())
        .collect();

    let searched = asciimoji::search_name(&asciimojis, &String::from("shrug")); 
    for ascimoji in searched {
        assert_eq!(&vec![194, 175, 92, 95, 40, 227, 131, 132, 41, 95, 47, 194, 175], &ascimoji.clone().into_bytes())
    }


}
