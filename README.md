# ASCIImoji
Use this command line tool to search inside a local, editable file one or more asciimoji.

# Install
The `.asciimojis.yml` is located in your home directory.
This file is downloaded from the repository so at the first run, or if the file is missing, the program could be affected by your Internet speed. 

## Crates.io
`cargo install asciimoji`

## Brew
`brew install jjocram/homebrew-asciimoji/asciimoji`

> The formulae definition can be found at [https://github.com/jjocram/homebrew-asciimoji](https://github.com/jjocram/homebrew-asciimoji)

# How to use
To search the shrug asciimoji ¯\\\_(ツ)\_/¯, for example:

`asciimoji shrug`
