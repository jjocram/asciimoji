# Contributing
Contributions are welcome, and they are greatly appreciated

# Types of contributions

## Report bugs
You can use GitLab to report a bug you find using ASCIImoji. 

Report bug at this link: [https://gitlab.com/jjocram/asciimoji/issues](https://gitlab.com/jjocram/asciimoji/issues)


If you are reporting a bug add some iformation about:
- Your operating system 
- How you installed ASCIImoji
- Detailed steps to reproduce the bug

## Suggest/Implement features
If you have some ideas about improving ASCIImoji feel free to suggest it. Add a new [issue](https://gitlab.com/jjocram/asciimoji/issues) with your idea or fork the project, edit it and make a merge request. 

## Improve asciimoji list
Create an [issue](https://gitlab.com/jjocram/asciimoji/issues) with your suggestion or make it forking the project, editing the asciimojis.yml file and making a merge reqeust. 
