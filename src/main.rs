use std::env;
use asciimoji::{Asciimoji, read_asciimojis_from_file, search_name};
use asciimoji::downloader::{update_yaml_file_form_repository, download_yaml_file_from_repository};
use clipboard::{ClipboardProvider, ClipboardContext};

use tabular::{Table, Row};

fn usage() {
    println!("ASCIImoji - {}\n{}\n", env!("CARGO_PKG_VERSION"), env!("CARGO_PKG_DESCRIPTION"));
    println!("usage:\tasciimoji <asciimoji name>\n");
    println!("Arguments:");
    println!("\t-l\tlist all the available asccimojis with their relative names\n");
    println!("\t-u\toverwrite your .asciimoji file with the latest from the repository\n");
    println!("\t-c\tcopy the first Asciimoji directly to the clipboard\n");
    println!("Edit asccimojis: asciimojis and asciimojis names are stored in a local file in your home directory ({}) called .asciimoji. It is a YAML file and an entry has the following format:\n", env::var("HOME").unwrap_or_else(|_| String::from("home directory not setted.")));
    println!("\t- names:\n\t  - \"name1\"\n\t  - \"name2\"\n\t  moji: asciimoji");
}

fn print_asciimoji(config_home: &str) {
    let mut asciimojis: Vec<Asciimoji> = read_asciimojis_from_file(config_home).unwrap();
    asciimojis = asciimojis
        .into_iter()
        .map(|asciimoji| asciimoji.names_to_lowercase())
        .collect();

    let mut args = env::args();
    args.next();

    for arg in args {
        let searched = search_name(&asciimojis, &arg.to_lowercase()); 
        if !searched.is_empty(){
            for ascimoji in searched {
                println!("{}", ascimoji);
            }
        } else {
            println!("Asciimoji for {} not found. Try 'asciimoji -l' to list all available asciimojis.", arg);
        }
    }
}

fn print_list_of_asciimoji(config_home: &str) {
    let mut asciimojis: Vec<Asciimoji> = read_asciimojis_from_file(config_home).unwrap();
    asciimojis = asciimojis
        .into_iter()
        .collect();
    let mut table = Table::new("{:<} {:<}");

    for asciimoji in asciimojis {
        let mut names = String::new();
        for name in asciimoji.names() {
            names.push_str(name);
            names.push_str(&", ");
        }

        table.add_row(Row::new()
            .with_cell(asciimoji.moji())
            .with_cell(names));
    }

    print!("{}", table);
}

fn download_asciimoji_file(config_home: &str, update: bool) {
    if update {
        match update_yaml_file_form_repository(config_home) {
            Ok(_) => {println!("Update succesful.")},
            Err(e) => {println!("Error: {}", e)}
        }
    } else {
        match download_yaml_file_from_repository(config_home) {
            Ok(_) => {},
            Err(e) => println!("Error: {}", e)
        };
    }
}

fn copy_asciimoji_to_clipboard(config_home: &str) {
        let mut asciimojis: Vec<Asciimoji> = read_asciimojis_from_file(config_home).unwrap();
    asciimojis = asciimojis
        .into_iter()
        .map(|asciimoji| asciimoji.names_to_lowercase())
        .collect();

    let mut args = env::args();
    args.next(); //skip program name
    args.next(); //skip -c option
    let arg_opt = args.next(); //get the first argouments
    match arg_opt {
        Some(arg) => {
            let searched = search_name(&asciimojis, &arg.to_lowercase()); 
            if !searched.is_empty(){
                let asciimoji_searched = searched[0];
                let mut clipboard_provider: ClipboardContext = ClipboardProvider::new().ok().unwrap();
                match clipboard_provider.set_contents(asciimoji_searched.to_string()){
                    Ok(_) => println!("{} copied to clipboard", asciimoji_searched),
                    Err(e) => println!("Cannot copy to clipbopard.\n{}", e),
                }
            } else {
                println!("Asciimoji for {} not found. Try 'asciimoji -l' to list all available asciimojis.", arg);
            }
        }
        None => println!("You must provide at least one asciimoji to search."),
    }
}

fn main() {
    let config_home = env::var("XDG_CONFIG_HOME").or_else(|_| env::var("HOME").map(|home|format!("{}/.asciimojis", home)));
    let config_home = config_home.unwrap_or_else(|_| String::from("asciimojis.yml"));

    download_asciimoji_file(&config_home, false);

    let args: Vec<String> = env::args().collect();
    if args.len() == 1 {
        usage();
    } else {
        match &args[1][..] {
            "-l" => print_list_of_asciimoji(&config_home),
            "-u" => download_asciimoji_file(&config_home, true),
            "-c" => copy_asciimoji_to_clipboard(&config_home),
            "-h" => usage(),
            _ => print_asciimoji(&config_home),
        }
    }

}
