use serde::Deserialize;
use std::error::Error;
use std::fs::File;
use std::io::BufReader;
use std::path::Path;

///Struct that contains the informations about asciimojis.
#[derive(Deserialize, Debug)]
pub struct Asciimoji {
    names: Vec<String>,
    moji: String,
}

impl Asciimoji {
    ///This function edits the names Vec into a Vec of Strings in lowercase and return the new
    ///object.
    pub fn names_to_lowercase(mut self) -> Asciimoji{
        self.names = self.names.into_iter().map(|name| name.to_lowercase()).collect();
        self
    }

    ///This function returns in an immutable way the list of names
    pub fn names(& self) -> &Vec<String> {
        &self.names
    }

    ///This function returns the moji icon
    pub fn moji(& self) -> &String {
        &self.moji
    }
}

///This function reads the yaml file from path and return a Result containing the Vec<Asciimoji>
///that will be used to search and print the asciimoji.
pub fn read_asciimojis_from_file<P: AsRef<Path>>(path: P) -> Result<Vec<Asciimoji>, Box<dyn Error>> {
    let yaml = serde_yaml::from_reader(BufReader::new(File::open(path)?))?;
    Ok(yaml)
}

///This function search in the Vec<Asciimoji> the name of of the asciimoji to show. It returns a
///Vec of String in case multiple asccimojis have that name.
pub fn search_name<'a>(asciimojis: &'a[Asciimoji], name: &str) -> Vec<&'a String> {
    asciimojis
        .iter()
        .filter(|ascimoji| ascimoji.names.contains(&name.to_string()))
        .map(|ascimoji| &ascimoji.moji)
        .collect()
}

pub mod downloader {
    use std::error::Error;
    use curl::easy::Easy;
    use std::fs::{File, remove_file};
    use std::io::prelude::*;
    use std::path::Path;

    const YAML_URL: &str = "https://gitlab.com/jjocram/asciimoji/-/raw/master/asciimojis.yml";

    ///This function is used to create the yaml file (if missing) used to store the list oi
    ///asciimoji
    pub fn download_yaml_file_from_repository(yaml_path: &str) -> Result<(), Box<dyn Error>> { 
        if Path::new(yaml_path).exists(){
            return Ok(());
        }
        let yaml_file = File::create(yaml_path).unwrap();    
        dowload_file(yaml_file)
    }
    
    ///This function is used to update the yaml file used to store the list of asciimoji
    pub fn update_yaml_file_form_repository(yaml_path: &str) -> Result<(), Box<dyn Error>> {
        remove_file(yaml_path)?;
        let yaml_file = File::create(yaml_path).unwrap();
        dowload_file(yaml_file)
    }

    ///This function download the yaml file that contains the list of asciimojis from the YAML_URL
    ///and save its content in the file located at the YAML_PATH.
    fn dowload_file(mut yaml_file: File) -> Result<(),  Box<dyn Error>> {
        let mut easy = Easy::new();
        easy.url(YAML_URL)?;
        easy.write_function(move |data| {
            yaml_file.write_all(data).unwrap();
            Ok(data.len())
        })?; 
        match easy.perform() {
            Ok(_) => Ok(()),
            Err(e) => { 
                println!("{}", e);
                Err("Error in cotactin GitLab".into())
            }
        }

    }
}
